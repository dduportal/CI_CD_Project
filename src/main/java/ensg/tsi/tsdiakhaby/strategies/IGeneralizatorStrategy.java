package ensg.tsi.tsdiakhaby.strategies;

import java.util.List;

import ensg.tsi.tsdiakhaby.geometries.Geometry;
import ensg.tsi.tsdiakhaby.geometries.Point;

public interface IGeneralizatorStrategy {
    public List<? extends Geometry> generalize(List<Point> listPoints, double tolerance) ;
}
