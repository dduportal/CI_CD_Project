package ensg.tsi.tsdiakhaby.strategies;
/**
 * Classe permettant de définir la stratégie de généralisation basé sur l'agorithme de @see <a href="https://fr.wikipedia.org/wiki/Algorithme_de_Douglas-Peucker">Douglas Peucker</a>
 * @author tsdiakhaby
 * @version 1.1
 */
import java.util.ArrayList;
import java.util.List;

import ensg.tsi.tsdiakhaby.exceptions.NegativeOrNullToleranceException;
import ensg.tsi.tsdiakhaby.geometries.GeometryFactory;
import ensg.tsi.tsdiakhaby.geometries.Line;
import ensg.tsi.tsdiakhaby.geometries.Point;

public class DouglasPeuckerStrategy implements IGeneralizatorStrategy
{
	/**
     * Simplifie un ensemble de points en un nombre réduit de points
     * @param tab tableau de points avec l'algorithme de @see <a href="https://fr.wikipedia.org/wiki/Algorithme_de_Douglas-Peucker">Douglas Peucker</a>
     * @param tolerance double distance à partir de laquelle on supprime un point de la ligne
     * @return List<Line> une liste de line passant par les points restants
     */
	public static List<Line> douglasPeucker(Point[] tab, double tolerance) 
	{
		if(tolerance<=0)
		{
			throw new NegativeOrNullToleranceException(tolerance);
		}
		
		 GeometryFactory geomFactory = new GeometryFactory();
		 List<Line> myLines = new ArrayList<Line>();
		 double dmax = 0;
		 int index = 0;
		 
		 Point ptStart = tab[0];;
		 Point ptEnd = tab[tab.length-1];
		 Point[] tabPoints = {ptStart,ptEnd};
		 
		 Line segment = (Line) geomFactory.line(tabPoints);
		 
		//Trouve le point le plus éloigné d'un segment
		 for(int i = 1; i<tab.length-1; i++)
		 {
			 Point ptI =(Point) geomFactory.point(tab[i].getX(), tab[i].getY());
			 double d = ptI.distanceToLine(segment);
			 
			 if(d>dmax)
			 {
				 index = i;
				 dmax = d;
			 }
		 }
			//Si la distance dmax est supérieure au seuil, on simplifie
			  if(dmax > tolerance)
			  {
				  // Appel récursif de la fonction	
				    Point[] dst1 = new Point[index+1];
				    Point[] dst2 = new Point[tab.length - index];
				    System.arraycopy(tab, 0, dst1, 0, index+1);
				    System.arraycopy(tab, index, dst2, 0, tab.length-index);
				    
				    List<Line> subLinesLeft = douglasPeucker(dst1, tolerance);
				    List<Line> subLinesRight = douglasPeucker(dst2, tolerance);
				    
				    //Construit la liste des résultats à partir des résultats partiels
				    myLines.addAll(subLinesLeft.subList(0, subLinesLeft.size()));
				    myLines.addAll(subLinesRight.subList(0, subLinesRight.size()));
			  }

			  else
			  {// Tous les points sont proches → renvoie un segment avec les extrémités
				myLines.add(segment);
			  }
		return myLines;
	}

	/**
     *Cette méthode fait appel à la {@link #douglasPeucker(Point[] , double) douglasPeucker} method.
     * @return List<Line> une liste de line passant par les points restants
     */
	@Override
	public List<Line> generalize(List<Point> listPoints, double tolerance){
		Point[]  tabPoints = new Point[listPoints.size()];
		return douglasPeucker(listPoints.toArray(tabPoints), tolerance);
	}
}
