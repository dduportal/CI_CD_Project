package ensg.tsi.tsdiakhaby.shapewritter;
/**
 * Classe permettant d'écrire des lignes dans un fichier shapefile
 * @author tsdiakhaby
 * @version 1.1
 */
import java.util.ArrayList;
import java.util.List;

import org.geotools.feature.SchemaException;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.geometry.jts.GeometryBuilder;
import org.opengis.feature.simple.SimpleFeature;

import ensg.tsi.tsdiakhaby.domain.GeometryType;
import ensg.tsi.tsdiakhaby.geometries.Geometry;
import ensg.tsi.tsdiakhaby.geometries.Line;

public class LineWritter extends ShapeWritter {

	/**
	 * Constructeur d'un writter de lignes
	 * @param filePath String chemin vers le fichier de sortie
	 * @param outputFileName String nom du fichier de sortie
	 * @param type GeometryTYpe type de géométrie à écrire (GeometryType.Point,GeometryType.LineString,GeometryType.Polygone)
	 * @param epsg int le système de projection de la couche en sortie
	 * @throws SchemaException
	 */
	public LineWritter(String filePath, String outputFileName, GeometryType type, int epsg)
			throws SchemaException {
		super(filePath, outputFileName, type, epsg);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Cette fonction permet de construire des SimpleFeature de l'API JTS à partir des géométries de mon API
	 * @param geometries List<Geometry>  liste des géométries
	 * @return List<SimpleFeature>
	 */
	@Override
	public List<SimpleFeature> buildJTSFeature(List<? extends Geometry> listGeom) 
	{
		SimpleFeatureBuilder featureBuilder = new SimpleFeatureBuilder(this.getFeatureType());
        
		GeometryBuilder geomBuild = new GeometryBuilder();
		List<SimpleFeature> features = new ArrayList<>();
         
        for (Geometry geom : listGeom) 
        {
        	Line line = (Line ) geom;
        	featureBuilder.add(geomBuild.lineString(line.getCoordinate()[0].getX(),line.getCoordinate()[0].getY(),line.getCoordinate()[1].getX(),line.getCoordinate()[1].getY()));
        	SimpleFeature featureBuild = featureBuilder.buildFeature(null);
            features.add(featureBuild);
            
			features.add(featureBuild); 
		}
        return features;
	}
}
