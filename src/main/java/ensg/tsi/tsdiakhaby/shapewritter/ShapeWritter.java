package ensg.tsi.tsdiakhaby.shapewritter;
/**
 * Classe abstraite permettant d'implémenter les fonctions communes aux GeometryWritter 
 * @author tsdiakhaby
 * @version 1.1
 */
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.geotools.data.DataUtilities;
import org.geotools.data.DefaultTransaction;
import org.geotools.data.Transaction;
import org.geotools.data.collection.ListFeatureCollection;
import org.geotools.data.shapefile.ShapefileDataStore;
import org.geotools.data.shapefile.ShapefileDataStoreFactory;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.data.simple.SimpleFeatureStore;
import org.geotools.feature.SchemaException;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;

import ensg.tsi.tsdiakhaby.domain.GeometryType;

public abstract class ShapeWritter implements Iwritter 
{
	private String filePath;
	private String outputFileName;
	private int epsg;
	private GeometryType type;
	private SimpleFeatureType featureType;
	
	/**
	 * Constructeur d'un writter
	 * @param filePath String chemin vers le fichier de sortie
	 * @param outputFileName String nom du fichier de sortie
	 * @param type GeometryTYpe type de géométrie à écrire (GeometryType.Point,GeometryType.LineString,GeometryType.Polygone)
	 * @param epsg int le système de projection de la couche en sortie
	 * @throws SchemaException
	 */
	public ShapeWritter(String filePath, String outputFileName, GeometryType type,int epsg) throws SchemaException {
		this.filePath = filePath;
		this.outputFileName = outputFileName;
		this.epsg=epsg;
		
		this.type=type;
		SimpleFeatureType featureType = DataUtilities.createType(this.outputFileName, "the_geom:"+this.type+":"+this.epsg);
		this.featureType = featureType;
	}

	/**
	 * Récupère le type de la géométrie de la couche
	 * @return GeometryType
	 */
	public GeometryType getType() {
		return type;
	}

	/**
	 * Returne le schéma du fichier 
	 * @return SimpleFeatureType
	 */
	public SimpleFeatureType getFeatureType() {
		return featureType;
	}

	/**
	 * Ecrit les géométries dans la couche
	 * @param features List<SimpleFeature> liste des géométries
	 * @throws SchemaException
	 */
	public void writeFeature(List<SimpleFeature> features) throws SchemaException, IOException
	{
		//Création du fichier de sortie
		File output = new File(this.filePath+"/"+this.outputFileName+".shp");
       
        ShapefileDataStoreFactory dataStoreFactory = new ShapefileDataStoreFactory();

        //Création du schémas
        Map<String, Serializable> params = new HashMap<>();
        params.put("url", output.toURI().toURL());
        params.put("create spatial index", Boolean.TRUE);

        ShapefileDataStore newDataStore = (ShapefileDataStore) dataStoreFactory.createNewDataStore(params);
        
        newDataStore.createSchema(featureType);
        String typeName = newDataStore.getTypeNames()[0];
        SimpleFeatureSource outputFeatureSource= newDataStore.getFeatureSource(typeName);
        
        //Ecriture dans le fichier de sortie
        Transaction transaction = new DefaultTransaction("create");

        if (outputFeatureSource instanceof SimpleFeatureStore) 
        {
            SimpleFeatureStore featureStore = (SimpleFeatureStore) outputFeatureSource;
            SimpleFeatureCollection collection = new ListFeatureCollection(featureType, features);
            featureStore.setTransaction(transaction);
            try {
                featureStore.addFeatures(collection);
                transaction.commit();
            } catch (Exception problem) {
                problem.printStackTrace();
                transaction.rollback();
            } 
            
        } 
        else 
        {
            System.out.println(typeName + " does not support read/write access");    
        }
        
        transaction.close();
        newDataStore.dispose();
        
	}
	
	/**
	 * Cette fonction permet de construire des SimpleFeature de l'API JTS à partir des géométries de mon API
	 * @param geometries List<Geometry>  liste des géométries
	 * @return List<SimpleFeature>
	 */
	abstract List<SimpleFeature> buildJTSFeature(List<? extends ensg.tsi.tsdiakhaby.geometries.Geometry> geometries);
	
	/**
	 * Fonction écrivant la géométrie de nos objets dans le shapefile
	 * Elle fait appel à la méthode writeFeature ci-haut.
	 */
	@Override
	public void write(List<? extends ensg.tsi.tsdiakhaby.geometries.Geometry> geometries) 
	{
		List<SimpleFeature> features = this.buildJTSFeature(geometries);
		try {
			this.writeFeature(features);
		} catch (SchemaException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	

	
	
}
