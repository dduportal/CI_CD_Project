package ensg.tsi.tsdiakhaby.shapewritter;
/**
 * Interface de création des Writter 
 * @author tsdiakhaby
 * @version 1.1
 */
import java.util.List;

import ensg.tsi.tsdiakhaby.geometries.Geometry;

public interface Iwritter 
{
	/**
	 * Ecrit des géométries dans un shapefile
	 * @param geometries List<Geometry> liste des géométries
	 */
	public void write(List<? extends Geometry> geometries);
}
