package ensg.tsi.tsdiakhaby.geometries;
/**
 * Classe permettant de créer un point à partir d'abscisse et d'ordonnées
 * @author tsdiakhaby
 * @version 1.1
 */

import org.geotools.geometry.jts.GeometryBuilder;

public class Point extends AbstractGeometry
{
	private double x;
	private double y;
	
	/**
	 * Construit un point à partir d'une abscisse et d'une ordonnée
	 * @param  x double abscisse 
	 * @param  y double ordonnée 
	 */
	public Point(double x, double y)
	{
		this.x = x;
		this.y = y;
	}
	
	/**
	 * Construit un point à partir d'un objet coordonné
	 * @param  c Coordinate
	 */
	public Point(Coordinate c)
	{
		this.x = c.getX();
		this.y = c.getY();
	}
	
	/**
	 * Renvoie la valeur de l'abscisse du point
	 * @return  double l'abscisse du point 
	 */
	public double getX() {
		return this.x;
	}


	/**
	 * Renvoie la valeur de l'ordonnée du point
	 * @return  double ordonné du point 
	 */
	public double getY() {
		return y;
	}
	
	/**
	 * Renvoie la géométrie de l'objet
	 * @return  Geometrie 
	 */
	
	public double distanceToLine(Line line)
	{
		Point p1 = new Point(line.getCoordinate()[0].getX(),line.getCoordinate()[0].getY()); 
		Point p2 = new Point(line.getCoordinate()[1].getX(),line.getCoordinate()[1].getY()); 
		
		GeometryBuilder geomBuild = new GeometryBuilder();
		return geomBuild.point(this.getX(),this.getY()).distance(geomBuild.lineString(p1.getX(),p1.getY(),p2.getX(),p2.getY()));
		}
	
	/**	
	 * compare 2 points 
	 * @param pt Point à comparer avec le point courant
	 * @return un boolean true (vrai) si le point donné en paramètre est inférieur et false (faux) dans le cas contraire  
	 */
	public boolean isSup(Point pt)
	{
		return (this.getX()>pt.getX()) || (this.getX()==pt.getX() && this.getY()>=pt.getY());
	}

	/**	
	 * compare 2 points 
	 * @param pt Point à comparer avec le point courant
	 * @return un boolean true (vrai) si le point donné en paramètre est inférieur et false (faux) dans le cas contraire  
	 */
	public boolean equals(Point pt)
	{
		return (this.getX()==pt.getX() && this.getY()==pt.getY());
	}

	
	/**
	 * Renvoie le type de géométrie du point
	 * @return  String "Point" 
	 */
	@Override
	public String getType() {
		return "Point";
	}
	
	/**
	 * Renvoie les coordonnées du point dans un tableau
	 * @return  Coordinate[] le tableau des coordonnées.
	 */
	@Override
	public Coordinate[] getCoordinate() {
		Coordinate[] coords = new Coordinate[1];
		coords[0]= new Coordinate(this.x,this.y);
		return coords;
	}

}
