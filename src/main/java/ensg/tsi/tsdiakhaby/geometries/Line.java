package ensg.tsi.tsdiakhaby.geometries;
/**
 * Classe permettant de créer une ligne à partir des points
 * @author tsdiakhaby
 * @version 1.1
 */
import java.util.ArrayList;
import java.util.List;

public class Line extends AbstractGeometry
{
	private List<Point> points = new ArrayList<Point> ();

	/**
	 * Construit une line à travers une liste de points
	 * @param  lsPoints List<Point> la liste des points pour construire la ligne
	 */
	public Line(List<Point> lsPoints) 
	{ 
		this.points = lsPoints;	
	}
	
	/**
	 * Construit une line vide
	 */
	public Line(){ }
	
	/**
	 * Construit une line à travers un tableau de points
	 * @param  lsPoints Point[] le tableau contenant les points pour construire la ligne
	 */
	public Line(Point[] lsPoints) 
	{ 
		for (Point point : lsPoints) {
			this.points.add(point);
		}
        	
	}
	/**
	 * Renvoie le type de géométrie de l'objet
	 * @return  String  
	 */
	@Override
	public String getType() {
		return "LineString";
	}

	
	/**
	 * Renvoie l'ensemble des coordonnées de la ligne
	 * @return  Coordinate[] le tableau des coordonnées.
	 */
	@Override
	public Coordinate[] getCoordinate() {
		Coordinate[] tabCoords = new Coordinate[this.points.size()];
        for(int i = 0; i<this.points.size();i++)
        {
        	tabCoords[i] = new Coordinate(this.points.get(i).getX(),this.points.get(i).getY());
        }
        return tabCoords;
	}
	
	/**
	 * Renvoie l'ensemble des points de la ligne
	 * @return  Point[] le tableau des points.
	 */
	public Point[] getPoints() {
		Point[] points = new Point[this.points.size()];
		int i =0;
		for (Point point : this.points) {
			points[i]=point;
			i++;
		}
		return points;
	}
	
	/**
	 * Renvoie l'ensemble des points de la ligne
	 * @return  List<Point>.
	 */
	public List<Point> getListPoints() {
		
		return this.points;
	}
	
	/**
	 * fusionne un ensemble de ligne
	 * @return  Line.
	 */
	public static Line mergeLines(List<? extends Geometry> list) {
		List<Point> points = new ArrayList<Point>();
		int i = 0;
		for (Geometry line : list) {
			for(Point p : ((Line) line).getPoints())
			{
				System.out.println(i);i++;
				points.add(p);
			}	
		}
		return new Line(points);
	}
	
	/**
	 * Renvoie le nombre de points de la ligne
	 * @return  int la taille de la liste de points
	 */
	public int getNbPoints() {
		return this.points.size();
	}
	
	
}
