package domain;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import ensg.tsi.tsdiakhaby.geometries.Coordinate;
import ensg.tsi.tsdiakhaby.geometries.Geometry;
import ensg.tsi.tsdiakhaby.geometries.GeometryFactory;
import ensg.tsi.tsdiakhaby.geometries.Line;
import ensg.tsi.tsdiakhaby.geometries.Point;
import ensg.tsi.tsdiakhaby.geometries.Polygone;

public class GeometryFactoryTest {
	
	static List<Point> listPoint;
	static GeometryFactory geomFact;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception 
	{
		geomFact = new GeometryFactory();
		Point point1 = Mockito.mock(Point.class);
		Point point2 = Mockito.mock(Point.class);
		Point point3 = Mockito.mock(Point.class);
		Point point4 = Mockito.mock(Point.class);
		Point point5 = Mockito.mock(Point.class);
		
		Mockito.when(point1.getX()).thenReturn(1.8);
		Mockito.when(point1.getY()).thenReturn(1.0);
		
		Mockito.when(point2.getX()).thenReturn(2.0);
		Mockito.when(point2.getY()).thenReturn(2.0);
		
		Mockito.when(point3.getX()).thenReturn(3.0);
		Mockito.when(point3.getY()).thenReturn(3.0);
		
		Mockito.when(point4.getX()).thenReturn(8.0);
		Mockito.when(point4.getY()).thenReturn(1.0);
		
		Mockito.when(point5.getX()).thenReturn(11.0);
		Mockito.when(point5.getY()).thenReturn(20.5);
				
		listPoint = new ArrayList<Point>();
		listPoint.add(point1);
		listPoint.add(point2);
		listPoint.add(point3);
		listPoint.add(point4);
		listPoint.add(point5);	
	}

	@Test
	public void testPointDoubleDouble() 
	{
		Geometry geom = geomFact.point(12.5, 13.5);
		assertTrue(geom instanceof Point);
	}

	@Test
	public void testPointCoordinate() 
	{
		Coordinate coord = Mockito.mock(Coordinate.class);
		
		Mockito.when(coord.getX()).thenReturn(1.8);
		Mockito.when(coord.getY()).thenReturn(1.0);
		
		Geometry geom = geomFact.point(coord);
		assertTrue(geom instanceof Point);
	}

	@Test
	public void testLineListOfPoint() {
		Geometry geom = geomFact.line(listPoint);
		assertTrue(geom instanceof Line);
	}

	@Test
	public void testLine() {
		Geometry geom = geomFact.line();
		assertTrue(geom instanceof Line);
	}

	@Test
	public void testPolygoneVide() {
		Geometry geom = geomFact.polygone();
		assertTrue(geom instanceof Polygone);
	}

	
	@Test
	public void testPolygone() {
		Geometry geom = geomFact.polygone(listPoint);
		assertTrue(geom instanceof Polygone);
	}

}
