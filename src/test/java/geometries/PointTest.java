package geometries;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.geotools.geometry.jts.GeometryBuilder;
import org.junit.BeforeClass;
import org.junit.Test;

import ensg.tsi.tsdiakhaby.geometries.Line;
import ensg.tsi.tsdiakhaby.geometries.Point;

public class PointTest {

	static Point point;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception
	{
		point = new Point(42.5,43.6);
	}
	@Test
	public void testGetX() {
		assertEquals(42.5, point.getX(),0);
	}

	@Test
	public void testGetY() {
		assertEquals(43.6, point.getY(),0);
	}

	@Test
	public void testCompare() {
		Point p = new Point(2.55,3.02);
		assertTrue(p.isSup(new Point(2.05,3.0223456789)));
		}
	@Test
	public void testDistanceToLine() {
		
		Point p1 = new Point(2.52,3.53);
		Point p2 = new Point(30.2,36.02);
		
		GeometryBuilder geomBuild = new GeometryBuilder();
		double dJTS = geomBuild.point(point.getX(),point.getY()).distance(geomBuild.lineString(p1.getX(),p1.getY(),p2.getX(),p2.getY()));
		
		List<Point> tabP = new ArrayList<Point>();
		tabP.add(p1); tabP.add(p2);
		Line line = new Line(tabP);
		double myDist = point.distanceToLine(line);
		assertTrue(dJTS == myDist);
		}
	
	@Test
	public void testGetType() {
		assertEquals("Point", new Point(20.5,12.3).getType());
	}

}
