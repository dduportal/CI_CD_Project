package geometries;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import ensg.tsi.tsdiakhaby.exceptions.NoEnoughPointsForPolygon;
import ensg.tsi.tsdiakhaby.geometries.Point;
import ensg.tsi.tsdiakhaby.geometries.Polygone;

public class PolygoneTest {

	static ArrayList<Point> listPoint = new ArrayList<Point>();
	static Polygone poly;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception
	{
		Point point1 = Mockito.mock(Point.class);
		Point point2 = Mockito.mock(Point.class);
		Point point3 = Mockito.mock(Point.class);
		Point point4 = Mockito.mock(Point.class);
		Point point5 = Mockito.mock(Point.class);
		
		Mockito.when(point1.getX()).thenReturn(1.8);
		Mockito.when(point1.getY()).thenReturn(1.0);
		
		Mockito.when(point2.getX()).thenReturn(2.0);
		Mockito.when(point2.getY()).thenReturn(2.0);
		
		Mockito.when(point3.getX()).thenReturn(3.0);
		Mockito.when(point3.getY()).thenReturn(3.0);
		
		Mockito.when(point4.getX()).thenReturn(8.0);
		Mockito.when(point4.getY()).thenReturn(1.0);
		
		Mockito.when(point5.getX()).thenReturn(11.0);
		Mockito.when(point5.getY()).thenReturn(20.5);
				
		listPoint = new ArrayList<Point>();
		listPoint.add(point1);
		listPoint.add(point2);
		listPoint.add(point3);
		listPoint.add(point4);
		listPoint.add(point5);
		
		poly = new Polygone(listPoint);		
	}
	@Test
	public void testGetType() 
	{
		assertNotNull("Polygone",poly.getType());
	}

	@Test
	public void testPolygone() {
		assertNotNull(poly);
	}
	
	@Test(expected = NoEnoughPointsForPolygon.class)
	public void uknownMethodTest() {
		poly = new Polygone(listPoint.subList(0, 2));
	}
	@Test
	public void testGetNbPoints() {
		assertTrue(poly.getNbPoints()>=listPoint.size());
	}
	

}
