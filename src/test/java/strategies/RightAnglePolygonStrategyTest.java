package strategies;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import ensg.tsi.tsdiakhaby.geometries.Point;
import ensg.tsi.tsdiakhaby.geometries.Polygone;
import ensg.tsi.tsdiakhaby.strategies.RightAnglePolygonStrategy;
import ensg.tsi.tsdiakhaby.strategies.StrategyFactory;

public class RightAnglePolygonStrategyTest {

	static StrategyFactory factory;
	static List<Point> listPoint; 

	@BeforeClass
	public static void setUpBeforeClass() throws Exception 
	{
		Point point1 = Mockito.mock(Point.class);
		Point point2 = Mockito.mock(Point.class);
		Point point3 = Mockito.mock(Point.class);
		Point point4 = Mockito.mock(Point.class);
		Point point5 = Mockito.mock(Point.class);
		
		Mockito.when(point1.getX()).thenReturn(1.8);
		Mockito.when(point1.getY()).thenReturn(1.0);
		
		Mockito.when(point2.getX()).thenReturn(2.0);
		Mockito.when(point2.getY()).thenReturn(2.0);
		
		Mockito.when(point3.getX()).thenReturn(3.0);
		Mockito.when(point3.getY()).thenReturn(3.0);
		
		Mockito.when(point4.getX()).thenReturn(8.0);
		Mockito.when(point4.getY()).thenReturn(1.0);
		
		Mockito.when(point5.getX()).thenReturn(11.0);
		Mockito.when(point5.getY()).thenReturn(20.5);
		
		listPoint = new ArrayList<Point>();
		listPoint.add(point1);
		listPoint.add(point2);
		listPoint.add(point3);
		listPoint.add(point4);
		listPoint.add(point5);
		
		factory = Mockito.mock(StrategyFactory.class);
		Mockito.when(factory.createStrategy("rightAnglePolygon")).thenAnswer(new Answer<Object>() {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
                return new RightAnglePolygonStrategy();
            }
        });
	}


	@Test
	public void testGeneralize() {
		RightAnglePolygonStrategy righAngleStrategy = (RightAnglePolygonStrategy)factory.createStrategy("rightAnglePolygon");
		Polygone p = (Polygone) (righAngleStrategy.generalize(listPoint, 0)).get(0);
		
		assertTrue(p.getNbPoints()<=listPoint.size());
	}

}
