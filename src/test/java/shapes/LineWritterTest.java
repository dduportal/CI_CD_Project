package shapes;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.geotools.feature.SchemaException;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;
import org.opengis.feature.simple.SimpleFeature;

import ensg.tsi.tsdiakhaby.domain.GeometryType;
import ensg.tsi.tsdiakhaby.geometries.Line;
import ensg.tsi.tsdiakhaby.geometries.Point;
import ensg.tsi.tsdiakhaby.shapewritter.GeometryWritterFactory;
import ensg.tsi.tsdiakhaby.shapewritter.LineWritter;

public class LineWritterTest {
	
	static String outPutPath;
	static String outputfileName;
	static GeometryType type;
	static int epsg;
	static LineWritter lineWritter ;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception 
	{
		outPutPath="/home/prof";
		outputfileName = "testFile";
		type = GeometryType.LineString;
		epsg = 2154;
		lineWritter =  new LineWritter(outPutPath,outputfileName,type, epsg);
	}

	@Test
	public void testLineWritter() throws SchemaException 
	{
		
		assertTrue(lineWritter!=null);
	}

	@Test
	public void testBuildJTSFeatureListOfQextendsGeometry() throws SchemaException 
	{
		Point point1 = Mockito.mock(Point.class);
		Point point2 = Mockito.mock(Point.class);
		Point point3 = Mockito.mock(Point.class);
		Point point4 = Mockito.mock(Point.class);
		Point point5 = Mockito.mock(Point.class);
		
		Mockito.when(point1.getX()).thenReturn(1.8);
		Mockito.when(point1.getY()).thenReturn(1.0);
		
		Mockito.when(point2.getX()).thenReturn(2.0);
		Mockito.when(point2.getY()).thenReturn(2.0);
		
		Mockito.when(point3.getX()).thenReturn(3.0);
		Mockito.when(point3.getY()).thenReturn(3.0);
		
		Mockito.when(point4.getX()).thenReturn(8.0);
		Mockito.when(point4.getY()).thenReturn(1.0);
		
		Mockito.when(point5.getX()).thenReturn(11.0);
		Mockito.when(point5.getY()).thenReturn(20.5);
		
		List<Point> listPoint = new ArrayList<Point>();
		
		listPoint.add(point1);
		listPoint.add(point2);
		listPoint.add(point3);
		listPoint.add(point4);
		listPoint.add(point5);
		Line line = new Line(listPoint);
		GeometryWritterFactory geomWFact = new GeometryWritterFactory();
		
		lineWritter = (LineWritter) geomWFact.createWritter(outPutPath, outputfileName, type, epsg);
		assertTrue(lineWritter.buildJTSFeature(Arrays.asList(line)).get(0) instanceof SimpleFeature);
	}

}
