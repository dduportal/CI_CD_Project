# PROJET CI_CD TSI 2017-2018
Dans ce projet, nous construisons un pipeline avec les étapes de build, test et deploy avec une intervention humaine pour approuver certaines actions.

# CI/CD avec GitLab
Pour faire un pipeline avec GitLab il faudra mettre à la racine du répertoire le fichier de configuration du pipeline avec le nom **.gitlab-ci.yml**. 
Ce fichier est constitué de sections décrivant les étapes du pipeline.

# Description du fichier de configuration de notre pipeline
**.gitlab-ci.yml**

**1** *Définition de l'image docker par défaut*
```yml
 image: maven:latest
```
Nous définissons l'image docker sur laquelle seront exécutées toutes les taches par défaut, ici nous choisissons la dernière version de l'image ***maven***.

**2** *Définition des variables globales*
```yml
variables:
  MAVEN_CLI_OPTS: "-s .m2/settings.xml --batch-mode"
  MAVEN_OPTS: "-Dmaven.repo.local=.m2/repository"
```
Dans ce projet, nous stockons toutes nos dépendances **maven** dans [Artifactory](https://jfrog.com/artifactory/)  pour qu'elles soient réutilisables prochainement par d'autres projets.

```yml
cache:
  paths:
    - .m2/repository/
    - target/
```
Tous les fichiers de compilation sont stockés le dossier *target*

**3** *Stage Build*
Dans cette étape (ou stage en anglais), on définit 2 tâches en parallèle un 1er build dans l'environnement **maven:latest** (celui par défaut) et un 2è build dans **maven:3-jdk-alpine**.
Les 2 étapes étant indépendantes donc la parallélisation permettrait d'optimiser le pipeline.
L'exécution de cette étape se fait par les scripts comme spécifié ci-dessous : 
```yml
build-maven-latest:
  stage: build
  script:
    - mvn $MAVEN_CLI_OPTS compile

build-maven-alpine:
  image: maven:3-jdk-8-alpine
  stage: build
  script:
    - mvn $MAVEN_CLI_OPTS compile
```
**4** *Stage Tests*
Cette étape a la même procédure que l'étape du build, les 2 tâches sont exécutées en parallèle.
```yaml
test-maven-latest:
  stage: test
  script:
    - mvn $MAVEN_CLI_OPTS test

test-maven-alpine:
  image: maven:3-jdk-8-alpine
  stage: test
  script:
    - mvn $MAVEN_CLI_OPTS test
```

**5** *Stage Deploy*
Cette étape est l'étape finale de notre pipeline, elle s'exécute après les 2 étapes précédentes (build & test). 
```yaml
deploy:
  stage: deploy
  script:
    - mvn $MAVEN_CLI_OPTS deploy
  when: always
  when: manual
  only:
    - master
```
Dans cette étape, nous rajoutons quelques contraintes: 
*  Cette étape s'exécutera dans tous les cas même si les étapes précédentes échouent
*  Le déploy doit approuver par une personne pour qu'il s'exécute
*  Seul la branche **master** a le droit de l'exécuter


**6** *Pull/Merge Request*

Le pull/merge request est géré par l'outil GitLab lui-même; En cochant la 1ère case de l'image ci-dessous, GitLab n'autorise 
le pull/merge request que si le pipeline en cours d'exécution est achevé; La 2è case indique à GitLab que le pull/merge request
n'est intégré que si toutes les discusions sont closes.


![alt text](https://gitlab.com/sadou/CI_CD_Project/raw/master/docs/merge_when_pipeline_succeeds_only_if_succeeds_settings.png)


**Réalisé par DIAKHABY Thierno Sadou**

**ENSG TSI 2017**


